# Terraform Proxmox Templater

This project'll create a entiere terraform project from a single inventory for proxmox

## Usage

Create a `.yaml` file and build your inventory following the section `BUILD INVENTORY` bellow

Install python dependencies

```Shell
pip3 install -r requirements.txt
```

Create the destination folder for the project

```Shell
mkdir folderProject
```

To create the terraform project, use the following command:

```Shell
python3 template_my_stack.py --all --loader [INVENTORY YAML] ./[DESTINATION FOLDER]
```

The directory `terraform_stack` will contain all .tf for differents ressources

## Build Inventory

Differents sections:



- PROXMOX (`REQUIRED`)
  - node (the name of the proxmox host (default pve))
- POOL (`REQUIRED`)
  - name (the name of the pool to create, all vm will be placed in this pool)
- DEFAULT (`REQUIRED`)
  - datastore (default datastore for all disks)
  - agent (Use the agent inside the template (default true))
  - account
    - user (Default user to use in the cloud-init)
    - key (SSH Publique key)
- VMs (`REQUIRED`)
  - name (give a name for the VM)
  - image_id (ID of the template you want use (default: 100))
  - cpu
    - core (number of core (default 2))
    - type (CPU type (default host))
  - memory
    - dedicated (Ammount of RAM/MB (default 512))
    - floating (number of floating RAM/MB not  (default 0))
  - disks (`REQUIRED`)
    - datastore_id (default datastore for this disk)
    - file_format (number of core (default 'raw'))
    - size (`REQUIRED` Disk )
  - account
    - user (Default user to use in the cloud-init)
    - key (SSH Publique key)
  - network
    - bridge (Use specific interface from proxmox server)
    - macaddress (Use specific Mac address)
    - vlan (Use specific ID for VLAN)

## Use the destination folder

The destiantion folder is ready to use,

Clone the `exemple.secrets.tfvars` to `secrets.tfvars` and complete variables

```Shell
cd [DESTINATION FOLDER]
cp exemple.secrets.tfvars secrets.tfvars
vi secrets.tfvars
```

Deploy infrastructure with the following commands:

If you use the docker container to launch terraform, spawn an terminal with the make command:

```Shell
make shell
```

```Shell
terraform init
terraform plan -var-file="secrets.tfvars"
terraform apply -var-file="secrets.tfvars"
```


## Troubleshooting

If you encounter some problems during deployment, delete VMs and recreate them (it's probably due to proxmox who can't create a large amount of VM in same time):

```Shell
terraform destroy -var-file="secrets.tfvars"
terraform apply -var-file="secrets.tfvars"
```
