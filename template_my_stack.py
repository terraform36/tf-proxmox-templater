import csv
import jinja2
import yaml
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--loader", dest="yaml_loader", help='yaml loader file')
parser.add_argument("--all", action='store_true', help='create compute, volumes, ports, port-attachments and subnets tf files')
parser.add_argument("--compute", action='store_true', help='create computes tf file')
parser.add_argument("--pool", action='store_true', help='create pool tf file')
parser.add_argument("output_dir", help='The output .tf directory files')
args = parser.parse_args()


def myrender(mydata, template_file, output_dir):
    template = templateEnv.get_template("templates/"+template_file)
    outputText = template.render(all=mydata)  
    writer = open(output_dir + '/' + template_file[:-3] + '.tf', "w")
    writer.write(outputText)
    print(f'{output_dir}/{template_file[:-3]}.tf created')
    return outputText

def copyFile(fileName, output_dir):
    template = templateEnv.get_template("templates/"+fileName)
    outputText = template.render()  
    writer = open(output_dir + '/' + fileName, "w")
    writer.write(outputText)
    print(f'{output_dir}/{fileName} created')

if __name__ == "__main__":
    templateLoader = jinja2.FileSystemLoader(searchpath="./")
    templateEnv = jinja2.Environment(loader=templateLoader)


    if args.yaml_loader:
        with open(args.yaml_loader) as f:
            dataMap = yaml.safe_load(f)

    myrender(dataMap, '00_variables.j2', args.output_dir)
    myrender(dataMap, '01_providers.j2', args.output_dir)
    copyFile('exemple.secrets.tfvars', args.output_dir)

    if args.compute or args.all:
        subnets = myrender(dataMap, '02_pool.j2', args.output_dir)

    if args.compute or args.all:
        subnets = myrender(dataMap, '05_compute.j2', args.output_dir)
